#![no_std]
#![no_main]

use embedded_hal::adc::OneShot;
use embedded_hal::digital::v2::InputPin; // Use input pins
use embedded_time::fixed_point::FixedPoint;
use panic_halt as _; // Ensure we halt the program on panic (if we don't mention this crate it won't be linked)
use rp_pico::entry; // The macro for our start-up function
use rp_pico::hal;
use rp_pico::hal::pac; // A shorter alias for the Peripheral Access Crate
use rp_pico::hal::pac::interrupt; // The macro for marking our interrupt functions
use rp_pico::hal::prelude::*; // A shorter alias for the Hardware Abstraction Layer // Use outpit pins

// USB Human Interface Device (HID) Class support
use usb_device::{class_prelude::*, prelude::*}; // USB Device support
use usbd_hid::descriptor::generator_prelude::*;
use usbd_hid::descriptor::MouseReport;
use usbd_hid::hid_class::HIDClass;

static mut USB_DEVICE: Option<UsbDevice<hal::usb::UsbBus>> = None; // The USB Device Driver (shared with the interrupt).
static mut USB_BUS: Option<UsbBusAllocator<hal::usb::UsbBus>> = None; // The USB Bus Driver (shared with the interrupt).
static mut USB_HID: Option<HIDClass<hal::usb::UsbBus>> = None; // The USB Human Interface Device Driver (shared with the interrupt).

/// Entry point to our bare-metal application.
///
/// The `#[entry]` macro ensures the Cortex-M start-up code calls this function
/// as soon as all global variables are initialised.
///
/// The function configures the RP2040 peripherals, then submits cursor movement
/// updates periodically.
#[entry]
fn main() -> ! {
    // Grab our singleton objects
    let mut pac = pac::Peripherals::take().unwrap();

    // Set up the watchdog driver - needed by the clock setup code
    let mut watchdog = hal::Watchdog::new(pac.WATCHDOG);

    // Configure the clocks
    //
    // The default is to generate a 125 MHz system clock
    let clocks = hal::clocks::init_clocks_and_plls(
        rp_pico::XOSC_CRYSTAL_FREQ,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    // Set up the USB driver
    let usb_bus = UsbBusAllocator::new(hal::usb::UsbBus::new(
        pac.USBCTRL_REGS,
        pac.USBCTRL_DPRAM,
        clocks.usb_clock,
        true,
        &mut pac.RESETS,
    ));
    unsafe {
        // Note (safety): This is safe as interrupts haven't been started yet
        USB_BUS = Some(usb_bus);
    }

    // Grab a reference to the USB Bus allocator. We are promising to the
    // compiler not to take mutable access to this global variable whilst this
    // reference exists!
    let bus_ref = unsafe { USB_BUS.as_ref().unwrap() };

    // Set up the USB HID Class Device driver, providing Mouse Reports
    let usb_hid = HIDClass::new(bus_ref, MouseReport::desc(), 60);
    unsafe {
        // Note (safety): This is safe as interrupts haven't been started yet.
        USB_HID = Some(usb_hid);
    }

    // Create a USB device with a fake VID and PID
    let usb_dev = UsbDeviceBuilder::new(bus_ref, UsbVidPid(0x16c0, 0x27da))
        .manufacturer("Slushee Labs") // Change all of this to your desire, it doesn't really matter
        .product("AwfulMouse")
        .serial_number("1.0.0")
        .device_class(0)
        .build();
    unsafe {
        // Note (safety): This is safe as interrupts haven't been started yet
        USB_DEVICE = Some(usb_dev);
    }

    unsafe {
        // Enable the USB interrupt
        pac::NVIC::unmask(hal::pac::Interrupt::USBCTRL_IRQ);
    };
    let core = pac::CorePeripherals::take().unwrap();
    let mut delay = cortex_m::delay::Delay::new(core.SYST, clocks.system_clock.freq().integer());

    let sio = hal::Sio::new(pac.SIO); // The single-cycle I/O block controls our GPIO pins

    // Set the pins to their default state
    let pins = hal::gpio::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    let mut adc = hal::Adc::new(pac.ADC, &mut pac.RESETS);

    let mut xpot = pins.gpio28.into_floating_input();
    let mut ypot = pins.gpio26.into_floating_input();

    let mut button = pins.gpio18.into_pull_up_input(); // This may vary on your joystick. The switch on mine whitched to ground

    let mut mousebutton: u8 = 0;
    let mut buttonstate: bool;
    let mut laststate: bool = false;

    loop {
        let analogx: u16 = adc.read(&mut xpot).unwrap(); // Reads X axis
        let analogy: u16 = adc.read(&mut ypot).unwrap(); // Reads Y axis

        buttonstate = button.is_low().unwrap(); // Reads button

        // State change detector
        if buttonstate != laststate {
            if button.is_low().unwrap() {
                mousebutton = 1; // position.buttons = 1 means the left button is pressed
            } else {
                mousebutton = 0; // position.buttons = 0 means left button is released
            }
        }
        laststate = buttonstate;

        let position = MouseReport {
            x: map(analogx, 4096, 100) - 40, // - 40 is an offset so when neutral the cursor doesn't drift
            y: -(map(analogy, 4096, 100) - 40), // The Y axis is inverted on the potentiometer
            buttons: mousebutton,
            wheel: 0,
            pan: 0,
        };

        push_mouse_movement(position).ok().unwrap_or(0); // Once the report struct has been filled, send it to the mouse

        delay.delay_ms(40);
    }
}

/// Submit a new mouse movement report to the USB stack.
///
/// We do this with interrupts disabled, to avoid a race hazard with the USB IRQ.
fn push_mouse_movement(report: MouseReport) -> Result<usize, usb_device::UsbError> {
    cortex_m::interrupt::free(|_| unsafe {
        // Now interrupts are disabled, grab the global variable and, if
        // available, send it a HID report
        USB_HID.as_mut().map(|hid| hid.push_input(&report))
    })
    .unwrap()
}

fn map(current: u16, oldmax: u16, newmax: u16) -> i8 {
    ((newmax as f32 / oldmax as f32) * current as f32) as i8
}

/// This function is called whenever the USB Hardware generates an Interrupt
/// Request.
#[allow(non_snake_case)]
#[interrupt]
unsafe fn USBCTRL_IRQ() {
    // Handle USB request
    let usb_dev = USB_DEVICE.as_mut().unwrap();
    let usb_hid = USB_HID.as_mut().unwrap();
    usb_dev.poll(&mut [usb_hid]);
}

# Awful mouse
## A mouse made with a joystick and a pi-pico
This mouse uses the rust rp-hal and the rp-pico rust crates to act as a HID device.
The button on the joystick acts as a right click. There is no left click, scroll wheel or fancy features. Just an awful mouse.

![Wiring diagram](/images/wiringdiagram.png)
![Awful Mouse](/images/awfulmouse.jpg)
